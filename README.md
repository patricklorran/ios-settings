# iOS Settings

Displays and manages the application settings from a property list file. Inspired by Ortwin Gentz's [InAppSettingsKit](https://github.com/futuretap/InAppSettingsKit).

## Installation

Settings is a Swift Package. To add it to your project, go to File > Swift Packages > Add Package Dependecy... and enter the package URL.

```
https://gitlab.com/patricklorran/ios-settings.git
```

## Usage

Create a Settings.plist file in your project's root directory describing the settings of your application. You can use [this example file](https://gitlab.com/-/snippets/2030608) as a staring point.

### Groups

Groups gather settings with related behaviour. Each item of the root array is a group of settings. The following keys are available:

| Key                       | Value            | Description              |
| ------------------------- | ---------------- | ------------------------ |
| `PLSettingsGroupHeader`   | Localized String | The group's header text. |
| `PLSettingsGroupFooter`   | Localized String | The group's footer text. |
| `PLSettingsGroupChildren` | Array            | The group's settings.    |

### Settings

Each setting is rendered and implemented according to its type. The following keys are available:

| Key                        | Required | Value                                                  | Description                                                  |
| -------------------------- | -------- | ------------------------------------------------------ | ------------------------------------------------------------ |
| `PLSettingsRowType`        | Yes      | `picker`, `detail`, `navigation`, `website` or `email` | The setting's type.                                          |
| `PLSettingsRowTitle`       | Yes      | Localized String                                       | The setting's title.                                         |
| `PLSettingsRowKey`         | No       | String                                                 | The setting's unique key. If the setting makes use of User Defaults, this key will be used for `get` and `set` operations. |
| `PLSettingsRowDetail`      | No       | String                                                 | The setting's detail text, displayed among its title.        |
| `PLSettingsRowChildren`    | No       | Array                                                  | The setting's child items, if its type is `picker`.          |
| `PLSettingsRowDestination` | No       | String                                                 | The setting's navigation destination, if its type is `navigation`, `website` or `email`. |

> **NOTE**: When using the `navigation` type, the value for `PLSettingsRowDestination` key is the name of the destination view controller class. It can be an Objective-C, Swift or `@objc`-annotated Swift class.

### Children (Picker only)

Child items are used within a `picker` type.

| Key                       | Value            | Description                                                  |
| ------------------------- | ---------------- | ------------------------------------------------------------ |
| `PLSettingsRowChildKey`   | String           | The item's unique key. It is used on `get` and `set` operations under User Defaults. |
| `PLSettingsRowChildValue` | Localized String | The item's title.                                            |

### Implementation

Wrap an instance of `SettingsViewController` in an `UINavigationController` and `push(_:animated:)` or `present(_:animated:)` it to display your defined settings.

```swift
let navigationController = UINavigationController(rootViewController: SettingsViewController())
present(navigationController, animated: true)
```

### Localization

The following localized strings are used by Settings. You must provide them in your **Localizable.strings** file.

| Localized Key                                          | Description                                                  | Example                                                      |
| ------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| `Settings`                                             | The settings view controller title.                          | "Settings"                                                   |
| `Settings.Support.Email.Prepare.Error.Title`           | The alert controller title. The alert is shown when an error occurs while preparing to send an e-mail. | "Error While Preparing Message"                              |
| `Settings.Support.Email.Prepare.Error.Message`         | The alert controller message. The alert is shown when an error occurs while preparing to send an e-mail. | "Something went wrong while preparing your message. Please try again later." |
| `Settings.Support.Email.Prepare.Error.Actions.Confirm` | The alert controller action title. The alert is shown when an error occurs while preparing to send an e-mail. | "OK"                                                         |

## Author Information

Developed by [Patrick Lorran](https://patricklorran.com).

If you find an issue feel free to contact me.

If you like my work consider [buying me a coffee](https://www.buymeacoffee.com/patricklorran).
