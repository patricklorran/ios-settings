# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.2] - 2020-10-16

### Changed

- Improve README details.

### Fixed

- Typos in README.

## [1.1.1] - 2020-10-16

### Changed

- Update README with implementation and localization instructions.

### Fixed

- `PickerViewController` cell reuse identifier was using a proprietary bundle identifier.
- Typos in README and on folder structure.

## [1.1.0] - 2020-10-16

### Added

- GitLab CI configuration file.

### Changed

- Improve Swift class initialization from string.
- Update README for better clarification.

## [1.0.0] - 2020-09-08

### Added
- Picker, detail, navigation, website and email setting types.
- Localized text support.