/*
 * MIT License
 *
 * Copyright (c) 2020 Patrick Lorran
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import UIKit

final class PickerViewController: UIViewController {
    
    // MARK: Constants
    
    private let pickerCellReuseIdentifier: String = "com.settings.pickerview.cell"
    
    // MARK: Properties
    
    private let userDefaults: UserDefaults = .standard
    private let notificationCenter: NotificationCenter = .default
    
    private var tableView: UITableView = .init(frame: .zero, style: .insetGrouped)
    
    private let preferenceKey: String
    private let items: [SettingItem]
    
    // MARK: Initialization
    
    init(key: String, items: [SettingItem]) {
        self.preferenceKey = key
        self.items = items
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented.")
    }
    
    // MARK: View controller lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
        
        navigationItem.largeTitleDisplayMode = .never
    }
    
    // MARK: Configuring the view
    
    private func configureTableView() {
        tableView.dataSource    = self
        tableView.delegate      = self
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: pickerCellReuseIdentifier)
        
        view.addSubview(tableView)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[tableView]|", metrics: nil, views: ["tableView": tableView]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[tableView]|", metrics: nil, views: ["tableView": tableView]))
    }
}

// MARK: - Table view data source

extension PickerViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: pickerCellReuseIdentifier, for: indexPath)
        
        let item = items[indexPath.row]
        let currentPreferenceValue = userDefaults.string(forKey: preferenceKey)
        
        cell.textLabel?.text = LocalizedString(item.value)
        cell.accessoryType = currentPreferenceValue == item.key ? .checkmark : .none
        
        return cell
    }
}

// MARK: - Table view delegate

extension PickerViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        userDefaults.set(items[indexPath.row].key, forKey: preferenceKey)
        notificationCenter.post(name: .settingsDidChangeNotification, object: nil, userInfo: ["key": preferenceKey])
        
        let indexPathsToReload = items
            .enumerated()
            .map({ IndexPath(row: $0.offset, section: 0) })
            .filter({ $0 != indexPath })
        
        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        tableView.deselectRow(at: indexPath, animated: true)
        tableView.reloadRows(at: indexPathsToReload, with: .automatic)
    }
}
