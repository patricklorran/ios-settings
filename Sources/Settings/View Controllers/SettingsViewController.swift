/*
 * MIT License
 *
 * Copyright (c) 2020 Patrick Lorran
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import MessageUI
import UIKit

/// A view controller that manages the application's settings.
final public class SettingsViewController: UIViewController {
    
    // MARK: Properties
    
    private let bundle: Bundle = .main
    private let userDefaults: UserDefaults = .standard
    private let notificationCenter: NotificationCenter = .default

    private var tableView: UITableView = .init(frame: .zero, style: .insetGrouped)
    
    private let viewModel: SettingsViewModel = .init()
    
    // MARK: View controller lifecycle
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
        
        viewModel.loadSettings()
        
        title = viewModel.title
        
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.largeTitleDisplayMode = .always
        navigationItem.rightBarButtonItem = .init(barButtonSystemItem: .close, target: self, action: #selector(close))
        
        notificationCenter.addObserver(self, selector: #selector(settingsDidChange), name: .settingsDidChangeNotification, object: nil)
    }
    
    // MARK: Configuring the view
    
    private func configureTableView() {
        tableView.dataSource    = self
        tableView.delegate      = self
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(tableView)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[tableView]|", metrics: nil, views: ["tableView": tableView]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[tableView]|", metrics: nil, views: ["tableView": tableView]))
    }
    
    // MARK: Selectors
    
    @objc
    private func close() {
        dismiss(animated: true)
    }
    
    @objc
    private func settingsDidChange() {
        tableView.reloadData()
    }
}

// MARK: - Table view data source

extension SettingsViewController: UITableViewDataSource {
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        viewModel.groups.count
    }
    
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        LocalizedString(viewModel.groups[section].header ?? .empty)
    }
    
    public func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        LocalizedString(viewModel.groups[section].footer ?? .empty)
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.groups[section].settings.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let setting = viewModel.groups[indexPath.section].settings[indexPath.row]
        let cell    = settingCell(for: setting)
        
        return cell
    }
    
    private func settingCell(for setting: Setting) -> UITableViewCell {
        var cell = UITableViewCell()
        
        switch setting.type {
            case .picker:
                cell = .init(style: .value1, reuseIdentifier: nil)
                cell.accessoryType = .disclosureIndicator
            
                if let preferenceKey = setting.key,
                   let currentPreferenceValue = userDefaults.string(forKey: preferenceKey),
                   let item = setting.items?.first(where: { $0.key == currentPreferenceValue }) {
                    cell.detailTextLabel?.text = LocalizedString(item.value)
                }
            
            case .detail:
                cell = .init(style: .value1, reuseIdentifier: nil)
                cell.detailTextLabel?.text = LocalizedString(setting.detail ?? .empty)
                cell.selectionStyle = .none
            
            case .navigation:
                cell.accessoryType = .disclosureIndicator
            
            case .website, .email:
                cell.textLabel?.textColor = .accent
                cell.textLabel?.textAlignment = .center
        }
        
        cell.textLabel?.text = LocalizedString(setting.title)
        
        return cell
    }
}

// MARK: - Table view delegate

extension SettingsViewController: UITableViewDelegate {
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let setting = viewModel.groups[indexPath.section].settings[indexPath.row]
        
        switch setting.type {
            case .picker:
                guard let key   = setting.key,
                      let items = setting.items else {
                    return
                }
                
                let pickerController = PickerViewController(key: key, items: items)
                pickerController.title = LocalizedString(setting.title)
                
                navigationController?.pushViewController(pickerController, animated: true)
            
            case .navigation:
                if let className = setting.destination,
                   let destinationController = bundle.classNamed(className) as? UIViewController.Type {
                    navigationController?.pushViewController(destinationController.init(), animated: true)
                } else if let executableName = bundle.infoDictionary?["CFBundleExecutable"] as? String,
                          let className = setting.destination,
                          let destinationController = bundle.classNamed("\(executableName).\(className)") as? UIViewController.Type {
                    navigationController?.pushViewController(destinationController.init(), animated: true)
                } else {
                    return
                }
            
            case .website:
                guard let destination = setting.destination,
                      let url = URL(string: destination) else {
                    return
                }
            
                UIApplication.shared.open(url, options: [:])
            
            case .email:
                guard let recipient = setting.destination else {
                    return
                }
            
                sendEmail(to: recipient)
            
            default:
                break
        }
    }
    
    private func sendEmail(to recipient: String) {
        if MFMailComposeViewController.canSendMail() {
            let mailController = MFMailComposeViewController()
            mailController.setToRecipients([recipient])
            mailController.mailComposeDelegate = self
            
            present(mailController, animated: true)
        } else {
            let title   = LocalizedString("Settings.Support.Email.Prepare.Error.Title")
            let message = LocalizedString("Settings.Support.Email.Prepare.Error.Message")
            let confirm = LocalizedString("Settings.Support.Email.Prepare.Error.Actions.Confirm")
            
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: confirm, style: .default))
            
            present(alert, animated: true)
        }
    }
}

// MARK: - Mail compose view controller delegate

extension SettingsViewController: MFMailComposeViewControllerDelegate {
    
    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}
